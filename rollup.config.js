import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import buble from 'rollup-plugin-buble'
import sourcemaps from 'rollup-plugin-sourcemaps'
import uglify from 'rollup-plugin-uglify'
import riot from 'rollup-plugin-riot'
import resolve from 'rollup-plugin-node-resolve'
import replace from 'rollup-plugin-replace'

const option = {
  ext: 'html'
}

export default {
  entry: 'src/main.js',
  dest: 'dist/ysvgmaps.js',
  sourceMap: true,
  moduleName: 'ysvgmaps',
  globals: {
    jquery: '$'
  },
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify( 'production' )
    }),
    riot(option),
    nodeResolve({ jsnext: true }),
    commonjs(),
    sourcemaps(),
    buble(),
    resolve({
      customResolveOptions: {
        moduleDirectory: 'node_modules'
      }
    })
//    uglify(),
  ],
  format: 'umd',
  external: ['fetch-everywhere', 'es6-promise']
}
