class Area {
  constructor (store, tag, context, value, descSvg, dataDesc, color, colorselect) {
    this.store = store
    this.id = tag.getAttribute('id')
    this.context = context
    this.tag = tag
    this.value = value === undefined ? 0 : value
    this.data_desc = dataDesc === null ? 'default' : dataDesc
    this.desc_svg = descSvg === null ? 'default' : descSvg
    this.color_in = colorselect === null ? '#00AEA0' : colorselect
    this.color_out = color === null ? '#BABABA' : color

    this.tag.addEventListener('mouseenter', (e) => {
      this.fill(true)
      this.store.dispatch( { type: 'SHOW_TOOLTIP', poly: this  } )
    })

    this.tag.addEventListener('mouseleave', (e) => {
      this.fill(false)
      this.store.dispatch( { type: 'HIDE_TOOLTIP'  } )
    })
    
    this.fill(false)
  }

  fill (isIn) {
    if (isIn) {
      this.tag.style.fill = this.color_in
    } else {
      this.tag.style.fill = this.color_out
    }
  }
}

export default Area
