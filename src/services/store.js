import { createStore, combineReducers } from 'redux'
import riot from 'riot'
import Data from '../models/Data.js'

function tooltip( state = { status: 'hidden' }, action ) {
  switch (action.type) {
    case 'SHOW_TOOLTIP':
      return { status: 'visible', poly: action.poly  }
    case 'HIDE_TOOLTIP':
      return { status: 'hidden'  }
    default:
      return state;
  }
}

function config(state = { status: 'waiting'  }, action) {
  switch (action.type) {
    case 'WAIT_CONFIG':
      return { status: 'waiting' }
    case 'LOAD_CONFIG':
      loadConfig(action.path, action.store)
      return { status: 'loading' }
    case 'CONFIG_LOADED':
      return { status: 'loaded', data: action.data }
    case 'FAIL_LOAD_CONFIG':
      return { status: 'fail', data: null  }
    case 'END_CONFIG':
      return { status: 'end', data: state.data  }
    default:
      return state
  }
}

function data(state = { status: 'waiting'  }, action = { data: {}  }) {
  switch (action.type) {
    case 'WAIT_DATA':
      return { status: 'waiting', data: action.data }
    case 'LOAD_DATA':
      loadData(action.path, action.store)
      return { status: 'loading' }
    case 'DATA_LOADED':
      return { status: 'loaded', data: action.data }
    case 'FAIL_LOAD_DATA':
      return { status: 'fail', data: null  }
    case 'UPDATE_DATA':
      return { status: 'updating', data: Object.assign({}, state.data, action.modif ) }
    case 'END_DATA':
      return { status: 'end', data: state.data }
    default:
      return state
  }
}

function loadData(path, store) {
  fetch(path)
    .then(function(response) {
      if (!response.ok) {
        store.dispatch({ type: 'FAIL_LOAD' })
        throw new Error('Bad response from server : ' + path + ' cannot be loaded')
      }
      return response.json()
    })
    .then(function(stories) {
      store.dispatch({ type: 'DATA_LOADED', data: stories  })
    })
}

function loadConfig(path, store) {
  fetch(path)
    .then( (response) => {
      if (!response.ok) {
        store.dispatch( {type: 'FAIL_LOAD_CONFIG'} )
        throw new Error('Bad response from server: ' + path + ' cannot be loaded.')
      }
      return response.json()
    })
    .then( (stories) => {
      if (stories.map == undefined) {
        store.dispatch( { type: 'FAIL_LOAD_CONFIG' } )
      }
      if (stories.legend == undefined) {
        store.dispatch( { type: 'FAIL_LOAD_CONFIG' } )
      }
      store.dispatch({ type: 'CONFIG_LOADED', data: stories  } )
    })

  }

const reducers = combineReducers({
  config,
  tooltip,
  data,
})



function create_our_store(reducer, configpath) {
  const store = createStore(reducer)
  store.subscribe(() => {
    riot.update()
  })
  return store
}

export {
  create_our_store,
  reducers,
  config,
}
