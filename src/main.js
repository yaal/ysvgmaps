import riot from 'riot'
import './components/ysvgmaps.tag.html'
import './components/ysvgmaps-map.tag.html'
import './components/ysvgmaps-date.tag.html'
import './components/ysvgmaps-legend.tag.html'
import './components/ysvgmaps-tooltip.tag.html'

export function ysvgmaps() {
  riot.mount('ysvgmaps')
}
