#!/bin/bash

if [ `find ../test -name "ysvgmaps.js*" | wc -l` -gt 0 ]; then\
  rm ../test/bundle.js*;\
fi

cp ../dist/ysvgmaps.js ../dist/ysvgmaps.js.map ../test/
