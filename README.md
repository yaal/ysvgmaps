# Ysvgmaps 

Usefull tool to use and config svg maps

Access on github : [ysvgmaps](https://github.com/yaal-fr/ysvgmaps)

---

# Dependencies

Ysvgmaps uses 2 mains technologies :

* [Redux](http://redux.js.org)
* [Riot](http://riotjs.com)

---

# How to use

First install ysvgmaps :

npm install --save-dev ysvgmaps

Then in js file : 

    // import ysvgmaps module
  
    import 'ysvgmaps'


    // start the installation of the maps 
  
    ysvgmaps.ysvgmaps()

ysvgmaps adds one tag for HTML. It needs a path to a config.
The path is gave in an attribute config in the tag like :

    <ysvgmaps config="path_to_your_config"></ysvgmaps>

---

# Configuration

    {
      "legend": {
        "automaticallysetlegend": true,
        "colors": ["#05F000", "yellow", "green", "blue",
        "parts" : [
          { "interval": 25, "color": "#05F000" },
          { "interval": 50, "color": "yellow" },
          { "interval": 100, "color": "green" },
          { "interval": 200, "color": "blue" }
        ],
        "title": "title of legend"
      },
      "map": {
        "nameOfArea": "name_of_area_in_svg",
        "title": "Title of the map",
        "selected-color": "green",
        "datadesc": "desc in tooltip", 
        "url": "path_to_svg",
        "data": "path_to_data"
      }
    }

The legend is defined by multiple parts. 'interval' is the superior value to set the color
at a area. 

If automaticallysetlegend is true, ysvgmaps will create a legend with the colors in colors variable.

The url is the link to your svg map in html tag.

Your svg data have to be formatted like :

    <svg>
        <name_of_area id="areaname" desc="Description" [...]/>	
    </svg>

Put the name of the area to attribute nameOfArea in the config file.

The data is the link to a json data for areas settings.
Example : if in your svg map they are 2 areas name: 'area1' and 'area2' with value 75 and 150:

    {
        "area1": 75,
        "area2": 150,
    }
